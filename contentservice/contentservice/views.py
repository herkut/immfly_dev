from rest_framework.views import APIView
from .models import Channel, Content
import json
from rest_framework.response import Response
from .serializers import ChannelSerializer, ContentSerializer
from rest_framework.decorators import api_view
from django.db import connection
import csv
import datetime


@api_view(http_method_names=["GET", "POST"])
def channel(request):
    if request.method == "POST":
        # JWT would be used to authorize an incoming request
        new_channel = Channel.objects.create(title=request.query_params.get("title"),
                                             language=request.query_params.get("language"),
                                             image_path=request.query_params.get("image_path"),
                                             parent_id=Channel.objects.get(id=int(request.query_params.get("parent_id"))))
        return Response({"message": "Channel has been created successfully"}, status=300)
    else:
        if request.query_params.get("channel_id"):
            channel_list = Channel.objects.filter(id=request.query_params.get("channel_id")) | Channel.objects.filter(
                parent_id_id=request.query_params.get("channel_id"))
        else:
            channel_list = Channel.objects.all()
        srlz = ChannelSerializer(channel_list, many=True)
        return Response(srlz.data)


@api_view(http_method_names=["GET", "POST"])
def content(request):
    if request.method == "POST":
        # JWT would be used to authorize an incoming request
        new_content = Content.objects.create(
            title=request.query_params.get("title"),
            type=request.query_params.get("type"),
            director=request.query_params.get("directory"),
            author=request.query_params.get("author"),
            genre=request.query_params.get("genre"),
            rating=request.query_params.get("rating"),
            content_path=request.query_params.get("content_path")
        )
        channel_id = request.query_params.get("channel_id")
        Channel.objects.get(id=channel_id).contents.add(new_content)
        return Response({"message": "Content has been successfully created"}, status=300)
    else:
        if request.query_params.get("content_id"):
            content_list = Content.objects.get(id=request.query_params.get("content_id"))
            srlz = ContentSerializer(content_list)
            return Response(srlz.data)

        elif request.query_params.get("channel_id"):
            with connection.cursor() as cursor:
                cursor.execute('''
                                with recursive C as  
                                (  
                                  select cc.id,  
                                         cc.title,
                                         cast('/' || cast(cc.Id as varchar) || '/' as varchar) Node,
                                         cc.id as RootID
                                  from contentservice_channel cc where cc.id=%s
                                  union all  
                                  select cc.id,  
                                         cc.title,
                                         cast(C.Node || CAST(cc.id as varchar) || '/' as varchar),
                                         C.RootID
                                  from contentservice_channel cc inner join C on cc.parent_id_id = C.id  
                                ) select C.id, C.title, array_agg(cc.id) from C 
                                        inner join contentservice_channel_contents ccc ON C.id=ccc.channel_id 
                                        inner join contentservice_content cc on ccc.content_id = cc.id 
                                        group by C.id, C.title
                                ''',
                               [request.query_params.get('channel_id')])
                rows = cursor.fetchall()
                channel_contents = []
                for row in rows:
                    contents = ContentSerializer(Content.objects.filter(id__in=row[2]), many=True).data
                    channel_contents.append({"channelId": row[0], 'channelTitle': row[1], 'contents': contents})
                return Response(channel_contents)

        else:
            content_list = Content.objects.all()
            srlz = ContentSerializer(content_list, many=True)
            return Response(srlz.data)


@api_view(http_method_names=["POST"])
def exportChannelRating(request):
    # JWT would be used to authorize an incoming request
    with connection.cursor() as cursor:
        cursor.execute('''
                        with recursive C as  
                        (  
                          select cc.id,  
                                 cc.title,
                                 cast('/' || cast(cc.Id as varchar) || '/' as varchar) Node,
                                 cc.id as RootID
                          from contentservice_channel cc
                          union all  
                          select cc.id,  
                                 cc.title,
                                 cast(C.Node || CAST(cc.id as varchar) || '/' as varchar),
                                 C.RootID
                          from contentservice_channel cc inner join C on cc.parent_id_id = C.id  
                        ) select * from contentservice_channel cc inner join (
                            select RootID, avg(rating) from C 
                                inner join contentservice_channel_contents ccc ON C.id=ccc.channel_id 
                                inner join contentservice_content cc on ccc.content_id = cc.id 
                                group by RootID) as r on cc.id=r.RootID order by avg desc
                        ''')
        rows = cursor.fetchall()
        ts = datetime.datetime.now().timestamp()
        with open('channelRatings_' + str(ts) + '.csv', 'w') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter='|', quotechar='"')
            for row in rows:
                csvwriter.writerow([row[1], row[6]])
        return Response({"message": "The channel ratings has been exported"}, status=300)
