from rest_framework import serializers
from .models import Channel, Content


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ['id', 'title', 'language', 'image_path']


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ['id', 'title', 'type', 'director', 'author', 'genre', 'rating', 'content_path']
