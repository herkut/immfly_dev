from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=100)
    types = [(1, 'Video'), (2, 'Audio'), (3, 'Pdf'), (4, 'Text')]
    type = models.PositiveSmallIntegerField(choices=types,
                                            default=2)
    director = models.CharField(max_length=250, blank=True, null=True, default=None)
    author = models.CharField(max_length=250, blank=True, null=True, default=None)
    genre = models.CharField(max_length=100, blank=True, null=True, default=None)
    rating = models.FloatField()
    content_path = models.CharField(max_length=500)


class Channel(models.Model):
    languages = [('EN', 'English'), ('TR', 'Turkish')]
    title = models.CharField(max_length=250)
    language = models.CharField(max_length=2, blank=True, null=True, choices=languages, default='EN')
    image_path = models.CharField(max_length=500, default=None, blank=True, null=True)
    contents = models.ManyToManyField(Content)
    parent_id = models.ForeignKey("self", blank=True, null=True, default=None, on_delete=models.CASCADE)


"""
class Group(models.Model):
"""