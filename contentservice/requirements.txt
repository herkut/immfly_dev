django==3.2
gunicorn
requests
djangorestframework
django-rest-swagger
django-filter
psycopg2-binary
debugpy

## dev requirements
sphinx
sphinx_rtd_theme
mock
responses
ipdb
ipython

## Test and quality analysis

pylint
coverage
django-jenkins
django-extensions
django-cors-headers
