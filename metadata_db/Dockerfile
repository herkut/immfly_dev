#
# example services.conf for https://docs.docker.com/engine/examples/postgresql_service/
FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=Europe/Barcelona

RUN apt update &&\
    apt install -y wget gnupg lsb-release build-essential git make &&\
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - &&\
    echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee  /etc/apt/sources.list.d/pgdg.list &&\
    apt update &&\
    apt -y install postgresql-12 postgresql-client-12 postgresql-contrib-12 postgresql-server-dev-12

# Run the rest of the commands as the ``postgres`` user
USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
RUN    /etc/init.d/postgresql start &&\
    psql --command "create user immfly_test WITH LOGIN ENCRYPTED PASSWORD '12345';" &&\
    psql --command "create database immfly_test OWNER immfly_test;"

# Insert dummy records

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
RUN echo "host postgres  all    0.0.0.0/0  reject" >> /etc/postgresql/12/main/pg_hba.conf
#RUN echo "host mtb_ard  mtb_ard    192.168.1.1/32  md5" >> /etc/postgresql/12/main/pg_hba.conf
RUN echo "host immfly_test   immfly_test    0.0.0.0/0  md5" >> /etc/postgresql/12/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/9.3/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/12/main/postgresql.conf

# Expose the PostgreSQL port
EXPOSE 5432

# Add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Set the default command to run when starting the container
# CMD ["/usr/lib/postgresql/12/bin/postgres", "-D", "/var/lib/postgresql/12/main", "-c", "config_file=/etc/postgresql/12/main/postgresql.conf"]
